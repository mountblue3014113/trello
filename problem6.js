const key = '02538ae4c8c1f2a4d09209fb1df4b5b0';
const token = 'ATTA3e884234a9ac8813a4b5c098eb8f627750118e791d215757aedb0f5d7dfe5981A3E6A47E';

const createBoard = (boardName) => {
    return fetch(`https://api.trello.com/1/boards/?name=${boardName}&key=${key}&token=${token}`, {
        method: 'POST'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to create board');
        }else{
            return response.json();
        }
    })
    .then(boardData => boardData.id)
    .catch(error => {
        console.error('Error creating board:', error);
    });
};

const createList = (boardId, name) => {
    return fetch(`https://api.trello.com/1/boards/${boardId}/lists?name=${name}&key=${key}&token=${token}`, {
      method: 'POST'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to create list');
        }else{
            return response.json();
        }
    })
    .then(listData => listData.id)
    .catch(error => {
        console.error('Error creating list:', error);
    });
};

const createCard = (listId, name) => {
    return fetch(`https://api.trello.com/1/cards?idList=${listId}&name=${name}&key=${key}&token=${token}`, {
        method: 'POST'
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Failed to create card');
        }else{
            return response.json();
        }
    })
    .then(cardData => cardData.id)
    .catch(error => {
        console.error('Error creating card:', error);
    });
};

const createBoardWithListsAndCards = (boardName) => {
    createBoard(boardName)
    .then(boardId => {
        const lists = ['John', 'Ron', 'Tom'];
        const listPromises = lists.map(listName => createList(boardId, listName));
        return Promise.all(listPromises);
    })
    .then(listIds => {
        const cardPromises = listIds.map((listId, index) => createCard(listId, `Card ${index + 1}`));
        return Promise.all(cardPromises);
    })
    .then(() => {
        console.log('Board with lists and cards created successfully!');
    })
    .catch(error => {
        console.error('Error creating board with lists and cards:', error);
    });
};

module.exports = createBoardWithListsAndCards;