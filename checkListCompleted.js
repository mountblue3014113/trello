const key = "02538ae4c8c1f2a4d09209fb1df4b5b0";
const token ="ATTA3e884234a9ac8813a4b5c098eb8f627750118e791d215757aedb0f5d7dfe5981A3E6A47E";

function getAllCheckList(boardId){
    return fetch(
        `https://api.trello.com/1/boards/${boardId}/checklists?key=${key}&token=${token}`,
        {
          method: "GET",
        }
      ).then((response) => {
        if(!response.ok){
            throw new Error(`Failed to get checklist from board !`)
        }else{
            return response.json();
        }
      })
}

function upadteCheckItems(itemsId,cardId){
    return fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${itemsId}?state=complete&key=${key}&token=${token}`, {
        method: 'PUT'
        })
        .then(response => {
            if(!response.ok){
                throw new Error(`Failed to update checkitems !`)
            }else{
                return response.json();
            }
        })
}

function getCheckItems(checkListId) {
  return fetch(
    `https://api.trello.com/1/checklists/${checkListId[0]}/checkItems?key=${key}&token=${token}`,
    {
      method: "GET",
    }
  )
    .then((response) => {
        if(!response.ok){
            throw new Error(`Failed to get checkitems !`)
        }else{
            return response.json();
        }
    })
    .then((checkitemdata) => {
      return Promise.all(
        checkitemdata.map((data) => upadteCheckItems(data.id, checkListId[1]))
      );
    });
}


function checkItmesCompleted(boardID) {
    return getAllCheckList(boardID)
      .then((checklists) => {
        const checkListId = checklists.map(data => [data.id, data.idCard]);
        const promises = checkListId.map((id) => getCheckItems(id));
        return Promise.all(promises);
      });
  }

module.exports = checkItmesCompleted;