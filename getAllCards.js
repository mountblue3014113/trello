const getCards = require('./getCards.js')
const key = '02538ae4c8c1f2a4d09209fb1df4b5b0';
const token = 'ATTA3e884234a9ac8813a4b5c098eb8f627750118e791d215757aedb0f5d7dfe5981A3E6A47E';


const getAllCards = (boardId) => {
    return fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`,
        {
          method: "GET",
          headers: {
            Accept: "application/json",
          },
        }
      )
        .then((response) => {
            if (response) {
                return response.json();  
            }else{
                throw new Error('Failed to fetch lists');
            }
        })
        .then((data)=>{
            const listIds = data.map(list => list.id);
            const promises = listIds.map(getCards)
            return Promise.all(promises)
            // console.log(data);
        })
        .then(allCards => {
            return allCards.flat();
        })
        .catch(error => {
            console.error('Error fetching board:', error);
        });
};

module.exports = getAllCards;
