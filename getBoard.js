const key = '02538ae4c8c1f2a4d09209fb1df4b5b0';
const token = 'ATTA3e884234a9ac8813a4b5c098eb8f627750118e791d215757aedb0f5d7dfe5981A3E6A47E';

const getBoard = (boardId) => {

    return fetch(`https://api.trello.com/1/boards/${boardId}?key=${key}&token=${token}`)
        .then((response) => {
            if (response) {
                return response.json();  
            }else{
                throw new Error('Failed to fetch board');
            }
        })
        .then((data)=>{
            resolve(data);
        })
        .catch((error) => {
            reject('Error fetching board:', error);
        });
};

module.exports = getBoard;
