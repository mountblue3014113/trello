const key = '02538ae4c8c1f2a4d09209fb1df4b5b0';
const token = 'ATTA3e884234a9ac8813a4b5c098eb8f627750118e791d215757aedb0f5d7dfe5981A3E6A47E';

const deleteListsFromBoard = (boardId) => {
        fetch(`https://api.trello.com/1/boards/${boardId}/lists?key=${key}&token=${token}`)
            .then(response => {
                if (!response.ok) {
                    throw new Error('Failed to fetch lists from board');
                }else{
                    return response.json();
                }
            })
            .then(lists => {
                const listsToDelete = lists.slice(0, 3);
                const deletePromises = listsToDelete.map(list => {
                    console.log(list.id)
                    return fetch(`https://api.trello.com/1/lists/${list.id}/closed?value=true&key=${key}&token=${token}`, {
                      method: 'PUT'
                    })
                });
                return Promise.all(deletePromises);
            })
            .then(() => {
                console.log('Lists deleted successfully!');
            })
            .catch(error => {
                console.error('Error deleting lists from board:', error);
            });
};

module.exports = deleteListsFromBoard;

