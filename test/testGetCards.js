const getCards = require("../getCards");
const listId = "663077a167d548f2267508d2";

getCards(listId)
    .then((cardsData) => {
        console.log("Cards Data:", cardsData);
    })
    .catch((error) => {
        console.error("Error:", error);
    });
