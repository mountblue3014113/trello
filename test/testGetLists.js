const getLists = require("../getLists.js");
const boardId = "663292abdd497b13c32ab536";

getLists(boardId)
    .then((listsData) => {
        console.log("Lists Data:", listsData);
    })
    .catch((error) => {
        console.error("Error:", error);
    }); 