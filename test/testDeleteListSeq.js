const deleteListsSequentially = require('../deleteListSeq')
const boardId = '6632a3f7ea1b6f7928a7796d';

deleteListsSequentially(boardId)
.then(() => console.log('Test passed: Lists fetched and deleted successfully!'))
.catch(error => console.error('Test failed:', error));