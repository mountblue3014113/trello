const getBoard = require("../getBoard.js");
const boardId = "663077a167d548f2267508ca";

getBoard(boardId)
    .then((boardData) => {
        console.log("Board Data:", boardData);
    })
    .catch((error) => {
        console.error("Error:", error);
    });