const createBoard = require("../createBoard");
const boardName = "Avishek Sah";

createBoard(boardName)
    .then((newBoardData) => {
        console.log("New Board Data:", newBoardData);
    })
    .catch((error) => {
        console.error("Error:", error);
    });