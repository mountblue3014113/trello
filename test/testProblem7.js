const deleteListsFromBoard = require('../problem7')
const boardId = '6632a05ca6407ea1f9e255d9';

deleteListsFromBoard(boardId)
    .then(() => {
        console.log('Test passed: Lists deleted successfully!');
    })
    .catch(error => {
        console.error('Test failed:', error);
    });
